<?php
/**
 * @author Jimmy Wang <1105235512@qq.com>
 * redis的增删改查 这只是一个简单的增删改查，大家感兴趣可以自己去扩展redis的其他功能，如list等
 * 根据自己的项目需求来扩展就行了
 * 兄弟要想跑起这个页面，需要安装你的redis，并且给php打上redis模块
 */
class RedisAction extends actionMiddleware
{   
    //查
    public function index()
    {   
	$data = R('user')->findAll();
	$this->display('redis/index.php',array('result'=>$data));
    }
    
    //添加数据
    public function add(){
	extract($this->input);
	if($isPost){
	    $data = array('name'=>$name,'addr'=>$addr, 'age'=>$age);
	    $insert = R('user')->add($data);
            if($insert){
                 $this->redirect("添加成功", Root.'redis/index');
            }
	   
	}
	$this->display('redis/add.php');
    }
    
    //删除数据
    public function delete(){
	extract($this->input);
	$re = R('user')->delete($id);
        if($re){
            $this->redirect("删除成功", Root.'redis/index');
        }
	
    }
    
    //修改数据
    public function edit(){
	extract($this->input);
	if($isPost){
	    $data = array('name'=>$name,'addr'=>$addr, 'age'=>$age);
	    $re = R('user')->edit($id, $data);
            if($re){
                $this->redirect("编辑成功", Root.'redis/index');
            }
	}
	$data = R('user')->findOne($id);
	$this->display('redis/edit.php',array('data'=>$data));
    }
    
    /**
     * 消息队列处理方式
     * 先把sql压入到redis，然后通过计划任务来执行sql插入点mysql
     */
    public function queue(){
	//此处sql是模拟sql
	$re = R()->lpush('sql', "update user set name='wsq' where user_id='10'");
	if(!$re){//如果压入失败或者redis服务器奔溃等情况，就把数据写入到文件中，保证数据完整性
	     file_put_contents('log/queue'.date('Y-m-d').'.txt', "update user set name='wsq' where user_id='10'",FILE_APPEND);
	}
	$re = R()->lpush('sql', "insert into user('name','age') values('wsq','20')");
	
	$data = R()->getlist('sql');
	foreach($data as $item){
	    $_sql = R()->rpop('sql');
	    var_dump($_sql);exit;//在此处执行sql
	}
    }
    
    public function test(){
        $redis = R();
//        $redis->zAdd('_zset1', 1, '22');
//        $redis->zAdd('_zset1', 2, '1');
//        $redis->zAdd('_zset1', 4, '55');
//        $redis->zAdd('_zset1', 6, '66');
        $data = $redis->hSet('admin','manage',['login'=>'2018-12-26','num'=>'15']);
        
        $data = $redis->hGet('admin','manage');
//        $data = $redis->hVals('admin');
//        $data = $redis->hdel('admin', 'manage');
        echo '<pre>';
        var_dump($data);exit;
    }



}