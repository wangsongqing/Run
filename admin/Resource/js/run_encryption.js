var encryptionJs = {
    //公钥串
    public_key :    'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6ryk5taxscQgSvn9nA9V4rLQAexs6+pzKOhR7EfizEIW9sX0XzhF8o7U9qYHGGUjIkz1RRPuelWrTXFtxURPgfEE4yWPLp3cupvi+AxEvzsBfdqdufXGb4ENOzMYS0TSu8qZ71Z8W/w6Erp1IlqPab5uON8mo3jIP2wFrHQIUUwIDAQAB',
    //公钥长度
    public_length : "10001",
	
	
   /**
    * 加密串
    * str 加密变量
    * @return bool
    */
    run_encryption:function(str){
      var encrypt = new JSEncrypt();
      encrypt.setPublicKey(encryptionJs.public_key);//公钥
      var encrypted = encrypt.encrypt(str);
      return encrypted;
    }
	
}