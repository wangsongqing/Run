# RunFramework
#### PHP+Mysql分布式框架
框架学习网址：http://www.yzmedu.com/course/103

#### 框架特点：

        1、简单配置就可以实现Mysql分表分库操作
        2、支持全项目memcache缓存，并支持数据更新刷新缓存
        3、支持redis增删改查
        4、支持redis消息队列
        5、可以用命令行运行框架，可以简单的实现计划任务和守护进程
        6、支持页面静态化
        7、可以很便捷的实现Mysql跨库操作
        8、所以框架可以快速的搭建数据库分布式(Mysql)和缓存集群(cache)，简单配置就可以支持千万级别的数据处理

#### 运行框架的环境：

        1、需要memcahce扩展,并且安装memcache在你的服务器(必须)
        2、需要pdo扩展(必须)
        3、需要openssl扩展(必须)
        4、如果可以最好把redis也安装上去，框架会有涉及redis的东西

#### 运行框架：
        1、以apache为例：
                    在apache配置虚拟主机
                    在httpd-vhost.conf里面添加

                    <VirtualHost 127.0.0.1:80>

                        #项目位置

                        DocumentRoot "C:/wamp/www/run/admin"

                        ServerName www.run.com

                        DirectoryIndex index.php index.html 

                    </VirtualHost>
        
        2、在hosts里面添加（host文件位置：C:\Windows\System32\drivers\etc\hosts）
           127.0.0.1       www.run.com
        
        3、如果页面显示空白或者报错，可以到/log/ 下面查看错误日志来排除错误
        
        4、进入sql文件夹，执行sql；然后运行框架就会进入登陆页面，登录用户名：admin 密码：123
#### 注意事项：

    1、框架缓存(cache)机制必须要明明白白，，必须要明明白白，必须要明明白白，重要的事情说三遍！！！！
       如果你对缓存机制不明白的多看看源码，多看看源码，多看看源码

    此框架调试的时候一定要注意缓存问题，在强调一下，一定要注意缓存，不然你会寸步难行，特别是调试的时候。

联系方式:1105235512@qq.com  
框架项目：http://blog.xmwme.com
