/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.53 : Database - run_user
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`run_user` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `run_user`;

DROP TABLE IF EXISTS `run_manage_log`;

CREATE TABLE `run_manage_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) unsigned NOT NULL,
  `admin_name` varchar(50) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `created` int(11) unsigned NOT NULL,
  `phone` varchar(11) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

/*Table structure for table `run_manage_user` */

DROP TABLE IF EXISTS `run_manage_user`;

CREATE TABLE `run_manage_user` (
  `admin_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `admin_name` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员帐号',
  `realname` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员真实姓名',
  `salt` varchar(6) DEFAULT NULL COMMENT '随机字段',
  `type` tinyint(1) unsigned NOT NULL COMMENT '后台类型  1管理后台，2运营后台，3财务后台',
  `mobile` varchar(11) DEFAULT NULL COMMENT '绑定电话',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属用户组ID',
  `group_name` varchar(24) NOT NULL DEFAULT '' COMMENT '所属用户组名',
  `operate_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作人ID',
  `operate_name` varchar(24) NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `created` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '账户状态，0停用，1正常，-1锁定',
  `operation_time` int(10) DEFAULT '0' COMMENT '最后操作时间',
  `lock_reason` varchar(50) DEFAULT NULL COMMENT '锁定原因',
  `lock_time` int(10) unsigned DEFAULT NULL COMMENT '锁定时间',
  `unlock_time` int(10) unsigned DEFAULT NULL COMMENT '解锁时间',
  PRIMARY KEY (`admin_id`),
  KEY `uId` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

/*Data for the table `run_manage_user` */

insert  into `run_manage_user`(`admin_id`,`admin_name`,`realname`,`salt`,`type`,`mobile`,`password`,`group_id`,`group_name`,`operate_id`,`operate_name`,`created`,`status`,`operation_time`,`lock_reason`,`lock_time`,`unlock_time`) values (1,'admin','run43','usmf',3,'18201197923','abc26eeaba5c78315e7f71019979c60e',13,'财务后台_财务',1,'admin',1489630596,1,0,NULL,NULL,NULL),(6,'wsqq','jimmy','pzvt',0,'15000000011','d1b6cc8b9cf12471f629cd43e98954e4',0,'',0,'',1499394940,1,0,NULL,NULL,NULL),(8,'wsq','李元芳benkui','ttjj',0,'18201197919','13d91a7e993d12fd586fe3cb6ba725a9',0,'yunying_dai',2,'',1499738202,1,0,NULL,888,NULL);

/*Table structure for table `run_user` */

DROP TABLE IF EXISTS `run_user`;

CREATE TABLE `run_user` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
  `age` int(2) DEFAULT '0' COMMENT '年龄',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `run_user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
